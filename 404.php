<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Contacto</title>

	<?php include('contenido/head.php'); ?>

</head>

<body><em></em>



	<?php include('chat.php'); ?>



	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

		<?php include('contenido/analytics.php'); ?>

		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">

				<div class="container">

					<h2>La Página que buscas no existe, Te dejamos nuestros datos de contacto.</h2>

				</div>

			</div>



			<!-- contact box -->

			<div class="contact-box">

				<div class="container">

					<div class="row">



						<div class="col-md-6" align="center">



							<div class="container">

								<div class="col-md-12" >

									<a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">

										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">

										</i> <font size="6"> WhatsApp</font> 

									</button>

								</a>

							</div>

						</div>



						<br>



						<div class="container">

							<div class="col-md-12" >

								<?php include('form.php'); ?>

							</div>

						</div>



					</div>



					<div class="col-md-3">

						<div class="contact-information">

							<h3>Información de Contacto</h3>

							<ul class="contact-information-list">

								<li><span><i class="fa fa-home"></i>Lateral Autopista México - Querétaro #2108</span> <span>Col. Parque Industrial Papanoa. </span> <span>Querétaro, Querétaro.</span></li>

								<li><span><i class="fa fa-phone"></i>(442) 309 4000</span></li>

								<li>

									<i class="fa fa-phone"></i><span>Taller de Servicio <strong>(442) 309 4009</strong></span><br>

									<i class="fa fa-phone"></i><span>Refacciones <strong>(442) 309 4006</strong></span><br>

									<i class="fa fa-phone"></i><span>Ventas <strong>(442) 309 4002</strong></span><br>

									<i class="fa fa-phone"></i><span>Recepción <strong>(442) 309 4000</strong></span><br>                                    

									<i class="fa fa-phone"></i><span>Seminuevos <strong>(442) 309 4001</strong></span><br>

									<i class="fa fa-phone"></i><span>Contabilidad <strong>(442) 309 4000 Opc. 5</strong></span><br>

								</li>

								<li><a href="#"><i class="fa fa-envelope"></i>recepcion@famecorregidora.com</a></li>

								<h3>Whatsapp</h3>

								<li><span><i class="fa fa-whatsapp"></i>

									<strong> 

										Ventas  |  

										<a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola,%20Quiero%20más%20información!" title="Ventas">

											4432732789

										</a>

									</strong>

								</span>

							</li>

							<li><span><i class="fa fa-whatsapp"></i>

								<strong> 

									Seguros |  

									<a href="https://api.whatsapp.com/send?phone=524432732277&text=Hola,%20Quiero%20más%20información!" title="Seguros">

										4432732277   

									</a>

								</strong>

							</span>

						</li>

						<li><span><i class="fa fa-whatsapp"></i>

							<strong> 

								Refacciones |  

								<a href="https://api.whatsapp.com/send?phone=524432732274&text=Hola,%20Quiero%20más%20información!" title="Refacciones">

									4432732274   

								</a> 

							</strong>

						</span>

					</li>

				</ul>

			</div>

		</div>



		<div class="col-md-3">

			<div class="contact-information">

				<h3>Horario de Atención</h3>

				<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Corregidora Querétaro</strong>; te escuchamos y atendemos de manera personalizada. </p>

				<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>

				<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

			</div>

		</div>



	</div>

</div>

</div>



<?php include('contenido/footer.php'); ?>





</body>

</html>