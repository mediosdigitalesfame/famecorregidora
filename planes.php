<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Planes</title>

	<?php include('contenido/head.php'); ?>

</head>

<body>



  <?php include('chat.php'); ?>



	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

    <?php include('contenido/analytics.php'); ?>

		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">         



				<div class="container">

					<h2>Planes y Comisiones</h2>

				</div>

			</div>



			<div class="about-box">

				<div class="container">

					<div class="row">

				</div>

			</div>







<!--barra información limpia-->

		<div class="section">

			<div id="about-section">



				<div class="welcome-box">

					<div class="container">

						<h1><span>Planes Financieros</span></h1><br>

						<p align="justify"><strong>GLOBAL CREDIT</strong> te ofrece los siguientes planes de financiamiento.</p><br><br>

                       

<p align="justify">

6, 12, 18, 24, 30, 36, 42, 48, 54, 60 y 72 meses</p><br><br>



<p align="left"><strong>Para vehículos seminuevos:</strong></p><br>                        



<p align="justify">

<div align="left">

<table width="50%" border="2" align="left">

  <tr valign="middle">

    <th scope="row"><strong>Año - Modelo</strong>&nbsp;</th>

    <td><strong>Plazos</strong>&nbsp;</td>

  </tr>

  <tr>

    <th scope="row">2006&nbsp;</th>

    <td>12 Meses&nbsp;</td>

  </tr>

  <tr>

    <th scope="row">2007&nbsp;</th>

    <td>12 - 36 Meses&nbsp;</td>

  </tr>

  <tr>

    <th scope="row">2008 - 2009&nbsp;</th>

    <td>12 - 48 Meses&nbsp;</td>

  </tr>

  <tr>

    <th scope="row">2010 - 2011&nbsp;</th>

    <td>12 - 60 Meses&nbsp;</td>

  </tr>

  <tr>

    <th scope="row">2010- 2016&nbsp;</th>

    <td>12 - 72 Meses&nbsp;</td>

  </tr>

</table>

</div>



</p><br><br>



<br><br><br>

<br><br><br>



						<h1><span>Comisiones</span></h1><br>

						<p align="justify"><strong>Tipo de Tasa: </strong> Fija sobre saldos insolutos.</p><br>                        

<p align="left"><strong>Enganche:</strong> Mínimo 10% o dependiendo de los planes por marca.</p><br>

<p align="left"><strong>Comisión por apertura:</strong> Desde 0% hasta 2% mas I.V.A. del monto financiado dependiendo del plan contratado por tipo de marca.</p><br>

<p align="left"><strong>Comisión por gastos de cobranza:</strong> $200.00 + IVA aplicado exclusivamente en el caso de presentar algún retraso en el pago de su mensualidad.</p><br>

<p align="left"><strong>Anticipos a Capital:</strong> Múltiplos de la mensualidad,

no hay penalización por pagos anticipados.</p><br>

<p align="left"><strong>Seguro Automotriz.</strong> Regionalizado con las modalidades de: <i>Anual de contado, anual financiado o multianual financiado al 100%</i></p><br>

<p align="left"><strong>Tipo de pago:</strong> Fijo durante toda la vida del crédito.</p><br>

                        

<br>







                	</div>

				</div>

                </div>

            </div>

        </div>

           



		 <?php include('contenido/footer.php'); ?>	 



</body>

</html>