<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>

<?php include('seguimientos.php'); ?>

	<title>FAME Honda Corregidora Querétaro</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="Title" content="FAME Honda Corregidora Querétaro">
	<meta name="description" content="Conéctate con el espíritu de la innovación automotriz e impulsa el poder de tus sueños en Honda Corregidora Querétaro. Piensa en auto, piensa en FAME.">
    <meta name="keywords" content="Honda Corregidora, honda fame, fame, Grupo Fame, Honda queretaro, fame queretaro, Queretaro, México, Autos queretaro, Nuevos honda, Seminuevos honda, seminuevos queretaro, seminuevos qro, seminuevos corregidora, Agencia, Servicio, Taller, Hojalatería, hojalateria, Pintura, postventa, accord, accord coupe, accord sedan, city, honda city, civic, civic coupe, civic sedan, civic si, civic hybrid, crosstour, cr-v, crv, cr-z, crz, fit, honda fit, odyssey, pilot, honda pilot, ridgeline, honda 2015, Lujo, honda 2014, The power of Dreams, ASIMO, Premium, 2014">
    <meta name="author" content="Grupo FAME División Automotriz">
    

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" media="screen">
    <link rel="icon" type="image/png" href="/images/favicon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">   

</head>
<body>

<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							 
							<span><i class="fa fa-phone"></i>Agencia: (442) 309 4000</span>
                            
						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/HondaFamee?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/HondaFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                          <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
 							<li><a class="whatsapp" href="http://hondacorregidora.com/contacto.php" target="_self"><i class="fa fa-whatsapp"></i></a></li>
                        
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>
							
							<li class="drop"><a href="#">Servicio</a>
								<ul class="drop-down">
                                    <li><a href="servicio.html">Servicio</a></li>
									<li><a href="servicio.php">Cita de Servicio</a></li>                     
									
									<li><a href="refacciones.php">Refacciones</a></li>
									<li><a href="privilegios.html">Club de Privilegios</a></li>
                                 </ul> </li>                                                             
							<li class="drop"><a class="active" href="autos.html">Autos</a>
								<ul class="drop-down">
                                
                                	<li><a href="fichas/fit.pdf" target="_blank">FIT</a></li>
                                	<li><a href="fichas/city.pdf" target="_blank">CITY</a></li>
                                	<li><a href="fichas/civic_sedan.pdf" target="_blank">CIVIC SEDÁN</a></li> 
                                   	<li><a href="civic-coupe.php" target="_self">CIVIC COUPÉ</a></li>
                                	<li><a href="fichas/accord_sedan.pdf" target="_blank">ACCORD </a></li>
                                	<li><a href="fichas/crv.pdf" target="_blank">CR-V</a></li>
                                   	<li><a href="fichas/hrv.pdf" target="_blank">HR-V</a></li>
                                   	<li><a href="fichas/pilot.pdf" target="_blank">PILOT</a></li>
                                   	<li><a href="fichas/odissey.pdf" target="_blank">ODYSSEY</a></li>
                                </ul> </li>
                                                      
							<li class="drop"><a href="promociones.html">Promociones</a>  
                            		<ul class="drop-down">
									<li><a href="promociones.html">Promociones Vigentes</a></li>
                                    <li><a href="finance.html">Honda finance</a></li>
									<li><a href="planes.html">Planes - Comisiones</a></li>
									<li><a href="autofinanciamiento.php" target="_self">Autofinanciamiento</a></li>																																				
                                  </ul>
                            
                              </li>                        
							
                            <li class="drop"><a href="seguros.php">Seguros</a>  
                            	   <ul class="drop-down">
                                   	<li><a href="seguros.php">Seguros</a></li>
				   					<li><a href="garantia.html">Garantía Extendida</a></li>																																			
                                  </ul>
                            
                              </li> 
                            
                            <li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>
							<li><a href="contacto.php">Contacto</a></li>
                            <li><a href="ubicacion.html">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">
        
        
<!-- ANALYTICS HONDA MONARCA DF-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47753362-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- FIN ANALYTICS -->        

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Civic Coupe 2016</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
<div class="single-project-content">
<div class="col-md-12">
<img src="images/civic-img1.jpg">
<img src="images/shadow.png">
</div></div>

 				<div class="col-md-6">
					<div class="services-post">
						<a class="services-icon2" href="../fichas/civic-coupe.pdf" target="_blank"><i class="fa fa-download"></i></a>
							<div class="services-post-content">
										<h4>+Información</h4>
										<p>Conoce más sobre el Civic Coupe 2016<br> descarga su <strong><a href="../fichas/civic-coupe.pdf" target="_blank">ficha técnica</a></strong></p><br>
                                        <p align="justify"><br>
                                        
                                    <h3> Tasa de 9.99%* o mensualidades <br>desde: $3,656.33** </h3>

										·Coupé con asombroso diseño y espacio interior.<br>
										·Conectividad superior.<br>
										·Smart Start Engine y Smart Entry Keyless<br>
                                        
                                        
                                        </p>                                        
                                        
					  </div>
				  </div>
							</div>
                                                      
                            
					  <div class="col-md-6">
						  <h3>Cotiza tu Nuevo Civic</h3>
                            
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "ventas@famecorregidora.com, gerencia@famecorregidora.com" . ','. "formas@grupofame.com" . ',' . ",";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Cotizador Civic Coupe Honda Corregidora";
			$asunto = "Cotizador Civic Coupe Honda Corregidora";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <?php if(isset($result)) { echo $result; } ?>
    </form>                            
                            
                           
                            
                    
					  </div>
                        
                        
                        
                        
<!-- Despues del Formulario.-->
<div class="container">
<div class="single-project-content">
	<h1>DISEÑO AUDAZ</h1>
		<h2>REDISEÑADO PARA LLEVARTE AL LIMITE</h2>
			
            <div class="col-md-3">
   			 <img src="images/civic-img6.jpg"><br>
    	 <p align="justify"><strong>Spoiler trasero</strong><br>
		Diseño extremo y funcional</p>
   	</div>
 
    <div class="col-md-3">
    <img src="images/civic-img3.jpg"><br>
     	<p align="justify"><strong>Rines de aluminio de 17” </strong><br>
						Provocador en cada detalle.
        										</p>
   	</div>
    
    <div class="col-md-3">
    <img src="images/civic-img4.jpg"><br>
   			 <p align="justify"><strong>Quemacocos eléctrico de 3 posiciones</strong> <br>
						Una bocanada de audacia.</p>
   	</div>
    
    <div class="col-md-3">
    <img src="images/civic-img5.jpg"><br>
    			<p align="justify"><strong>Luminosidad irresistible</strong><br>
							Luces LED de marcha diurna y faros de niebla.</p>
   	</div><br><br><br>
    

</div></div>


<div class="container">
	<div class="single-project-content">
		<div class="col-md-8">
		<img src="images/civic-img2.png">
        </div>
     <div class="col-md-4">
     <p align="justify">
     					<br><br><br><h1>Totalmente renovado</h1><br>
		Resultado de una reconstrucción total, Civic Coupé presume rasgos distintivos de Honda que lo hacen irresistible desde cualquier ángulo.</p>
     
    </div>
	
</div></div>


<div class="container">
<div class="single-project-content">
 <div class="col-md-4">
     <p align="justify">
     	<h1>INTERIOR CON ESTILO</h1><br>
              <h3>CONFORT Y FUNCIONALIDAD A DETALLE.</h3>
		·Disfruta cada uno de los detalles que lo<br> hacen único.<br> <br>
		·Consola central con compartimento y doble<br> portavasos. <br><br>
        ·Funcionalidad y espacio.</p>
     
    </div>
    <div class="col-md-8">
		<img src="images/civic-img8.png">
        </div>
	
</div></div>


<div class="container">
<div class="single-project-content">
 <div class="col-md-8">
		<img src="images/civic-motor.png">
        </div>
 
 <div class="col-md-4">
     <p align="justify">
     	<h1>INTRÉPIDO</h1><br>
              <h3>DESEMPEÑO EXTRAORDINARIO</h3>
		El motor de Civic Coupé es el impulso necesario<br> para lanzarte a donde quieras. Con su eficiente transmisión, el límite lo pones tú. <br><br>

<strong>Motor VTEC Turbo</strong> de 4 cilindros en línea.<br> Poder total de hasta 174 hp.<br><br>

<strong>Transmisión de velocidad Continuamente Variable</strong><br> (CVT) para una conducción suave y activa.<br><br>

<strong>Tecnología Earth Dreams y sistema Eco Assist®(1)</strong><br> que optimiza el rendimiento de combustible y <br>reduce las emisiones contaminantes. <br><br>
        
        </p>
     
    </div>
    
	
</div></div>



<div class="container">
	<div class="single-project-content">
    <div class="col-md-12">
    	<h1>Colores </h1><br>
    </div>
    	<div class="col-md-3">
        	<img src="images/civic-img9.png"><br>
            <p align="justify">
            	<strong>Exterior: Negro Cristal</strong><br>
                Con interiores: Gris* Marfil
                
			</p>
		</div>	
        
        <div class="col-md-3">
        	<img src="images/civic-img10.png"><br>
            <p align="justify">
            	<strong>Plata Lunar</strong><br>
                Gris* 
			</p>
		</div>	
        
         <div class="col-md-3">
        	<img src="images/civic-img11.png"><br>
            <p align="justify">
            	<strong>Rojo Extremo</strong><br>
                Marfil
			</p>
		</div>	
        
         <div class="col-md-3">
        	<img src="images/civic-img12.png">
            <p align="justify">
            	<strong>Verde Audaz</strong><br>
                Gris* 
			</p>
		</div>	
        
	</div>
    
    <p align="justify">
    * Interior en color gris disponible en verde audaz, plata lunar y las primeras 184 unidades de color negro cristal**.<br> 

**Para mayor información y disponibilidad del interior en color gris aplicable para las unidades negro cristal, visite a su Distribuidor Autorizado Honda más cercano.<br><br>
    </p>    
    </div>

<div class="container">
	<div class="single-project-content">
    	<p align="justify">
        	<a href="fichas/civic-coupe.pdf" target="_blank"><strong> Ver Ficha tecnica CIVIC COUPE 2016</strong></a>
        </p>
    </div>
</div>



<!--Fin del Contenido --->
					</div>
				</div>
			</div>

		</div><br><br><br><br><br>



		<!-- End content -->


		<!-- footer 
			================================================== -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i>  01800 670 8386 | </span> 2016 Honda Corregidora | <i class="fa fa-user"> </i><a href="aviso.html"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
</body>
</html>