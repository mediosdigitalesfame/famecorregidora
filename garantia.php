 <!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Garantía Extendida</title>

	<?php include('contenido/head.php'); ?>

</head>

<body>



	<?php include('chat.php'); ?>



	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

		<?php include('contenido/analytics.php'); ?>

		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">         



				<div class="container">

					<h2>Garantía Extendida</h2>

				</div>

			</div>



			<div class="about-box">

				<div class="container">

					<div class="row">

				</div>

			</div>







<!--barra información limpia-->

		

        <div class="section">

			<div id="about-section">



				<div class="welcome-box">

					<div class="container">

							

						<h1><span>EXTENSIÓN DE GARANTÍA Y CERTIFICADO DE GARANTÍA</span></h1><br>

                       <h2><span><strong>PROTECCIÓN AMECAH</strong></span> <br>Exclusiva para vehículos Honda<br><br></h2>

						

                

                <div class="h7"><strong>¿QUÉ ES?</strong></div><br>

                <p align="justify"> 

Es un contrato de servicio que le otorga la posibilidad de ampliar la garantía de fábrica de su vehículo Honda hasta por 6 años ó 125,000 km para vehículos nuevos; y para vehículos entre 10,000km y 100,000km podrá contratar esta cobertura hasta por 2 años ó 40,000km (Consultar requisitos de Elegibilidad).</p><br><br>

     			 

                <div class="h7"><strong>¿QUÉ CUBRE? </strong></div><br>

                <p align="justify">

                Una vez concluida la garantía del Fabricante por tiempo ó kilometraje (lo que suceda primero), usted estará cubierto contra las mismas averías que ampara la garantía original del fabricante quien cubre DEFECTOS DE FABRICACIÓN indicadas en la póliza de garantía del vehículo. (Aplican Exclusiones y Condiciones)<br><br>

                

Cubre la reparación en cualquier Concesionaria Honda de los conjuntos afectados, reparando o sustituyendo las piezas defectuosas de dicho conjunto, según lo requieran las exigencias técnicas; así como los costos que por mano de obra se originen. NO REQUIERE PAGO DE DEDUCIBLE.<br><br>



Incluye Asistencia Vial en México, Estados Unidos y Canadá. <br><br>



<strong>Coberturas Disponibles.</strong><br><br>



•Extensión de Garantía <strong>GOLD *</strong> (Autos Nuevos ó vehículos con menos de 10,000km ó 180 días)<br>

•Extensión de Garantia <strong>SILVER*</strong> (Autos con Historial de Servicio)<br>

•Certificado de Garantía <strong>PLUS* </strong>(Autos sin Historial de Servicio)<br><br>

*Consulte condiciones de elegibilidad.<br><br><br>



<strong>Plazos Disponibles adicionales a la garantía de fábrica.</strong><br><br>



•1 año ó 20,000 km* <br>

•2 años ó 40,000 km* <br>

•3 años ó 65,000 km* (Sólo para GOLD)<br><br>

*Lo que suceda primero.<br><br><br>

</p>



		 <div class="h7"><strong>¿QUIÉN RESPALDA LA COBERTURA? </strong></div><br>

         <p align="justify">Diseño y Respaldo por la Asociación Mexicana de Concesionarios Honda y Acura en México (AMECAH).</p><br><br><br>		



		 <div class="h7"><strong>¿QUÉ BENEFICIOS TIENE EL CONTRATARLO?</strong></div><br>

         <p align="justify">- Protección contra desembolsos imprevistos al terminar la garantía del fabricante, por ejemplo: reparaciones menores como fugas de aceite y complejas como el arreglo/sustitución de motor ó trasmisión en caso de ser necesario. (Requiere dictamen técnico por un concesionario Honda).<br><br>

         

- Como un beneficio exclusivo de Extensión de Garantía GOLD (Vehículos nuevos), solo en caso de pérdida total por daños materiales o robo total, se podrá trasferir a otro vehículo nuevo de la marca Honda. (Aplican condiciones)<br><br>



- En caso de vender el vehículo la cobertura continua con su Honda dando con ello mejor plusvalía.<br><br>

- Adquirirla con anticipación ahorrará la actualización de precios de esta cobertura por cuestiones de inflación anual y otros ajustes.<br><br>

- Incluye Asistencia Vial sin costo adicional<br><br>

</p><br><br><br>



			

<h1><span>PRECIOS  </span></h1><br><br>

				<div class="single-project-content">

                <div class="col-md-12">

                	<div class="h7"><strong>GARANTÍA GOLD</strong></div><br>

				<img alt="Precios de Garantia Extendida HONDA Corregidora" src="images/precio-garantia.jpg"><br><br>

                <div class="h7"><strong>GARANTÍA SILVER</strong></div><br>

                <img alt="Precios de Garantia Extendida HONDA Corregidora" src="images/precio-garantia-silver.jpg"><br><br>

                <div class="h7"><strong>GARANTÍA PLUS</strong></div><br>

                <img alt="Precios de Garantia Extendida HONDA Corregidora" src="images/precio-garantia-plus.jpg"><br><br>

                

               <p align="justify"> <strong>Los precios aquí mostrados son una referencia, por lo que tendrán que ser confirmados en la Concesionaria de tú elección.</strong><br></p>

                <p class="h7">CONTACTO/ INFORMACIÓN:Consulte exclusiones y condiciones e información en general en:<a href="www.extensiongarantia.com.mx"> www.extensiongarantia.com.mx </a></p>

               

               

               

               

               

               </div>

                         	</div>

 

        

</div>

                	</div>

				</div>

                

                </div>

                </div>

                



		<!-- footer 

			================================================== -->

		

<?php include('contenido/footer.php'); ?>



</body>

</html>