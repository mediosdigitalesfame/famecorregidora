<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Aviso de Privacidad</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
         <?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Aviso de Privacidad</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>


<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box2">
					<div class="container">
						<p><h4><strong>I. IDENTIFICACIÓN DEL RESPONSABLE</strong></h4><br>
                        
EEn Fame Corregidora S.A. de C.V. en lo sucesivo Honda Corregidora, con domicilio en Lateral Carretera México-Querétaro No. 2108 Col. Parque Comercial Papanoa; Querétaro, Qro. C.P. 76080; le comunicamos que los datos personales de nuestros clientes y clientes potenciales son tratados en atención a lo que dispone la legislación en la materia y de manera confidencial y segura. <br><br><br>

<h4><strong>II. MEDIOS DE OBTENCIÓN Y DATOS PERSONALES QUE SE RECABAN</strong></h4><br>

Honda Corregidora recaba los siguientes datos personales necesarios para dar cumplimiento a las finalidades del presente Aviso de Privacidad, dependiendo de la relación que con usted exista.<br><br>

<li><strong>A.</strong> Datos personales que recabamos de manera personal: Datos personales de identificación, laborales, patrimoniales, financieros, datos de terceros. (Al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).<br>
</li>

<li><strong>B.</strong>	Datos personales que recabamos de manera directa a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.</li>
</li><br>

<li><strong>C.</strong> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación. <br><br>

Se le informa que las imágenes y sonidos captados a través del sistema de video vigilancia, serán utilizadas para su seguridad y de las personas que nos visitan.
<br><br>
Si usted participa en eventos y reportajes, se le comunica que las imágenes y sonidos captados, podrán ser difundidas a través de medios de comunicación interna y las distintas redes sociales.
<br><br>
Asimismo, los sonidos relativos a su voz, recabados a través de los distintos medios utilizados, serán tratados como soporte del seguimiento de la calidad de nuestros servicios.
<br><br>
<strong>Datos personales sensibles</strong>
Aquellos que refieren a características personales, como huella digital, género, etc.

 
<br>
<br>
<h4><strong>III. FINALIDADES</strong></h4><br>

En términos de lo establecido por La Ley Federal de Protección de Datos Personales en Posesión de los Particulares (La Ley) y su Reglamento, los datos personales que nos proporcione serán utilizados para las siguientes finalidades:<br><br>

<li><strong>A.</strong>	Necesarias para la prestación del servicio:<br>


i.	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.<br><br>
ii.	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofinanciamiento.<br><br>
iii.	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).<br><br>
iv.	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.<br><br>
v.	Consultar el historial crediticio ante las sociedades de información crediticia.<br><br>
vi.	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.<br><br>
vii.	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva Honda de México S.A. de C.V. (llamada/recall), así como de actualizaciones técnicas.<br><br>
viii.	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.<br><br>
ix.	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el mantenimiento de vehículos.<br><br>
x.	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.<br><br>
xi.	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.<br><br>
xii.	Mantener actualizados nuestros registros para poder responder a sus consultas.<br><br>


</li><br>

<li><strong>B.</strong>	No necesarias para la prestación el servicio:<br>
1)	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece Honda de México S.A. de C.V.<br><br>
2)	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.<br><br>
Es importante mencionar que las finalidades (i), (ii), (iii) (iv), (v), (vi), (vii), (viii), (ix), (x), (xi) y (xii) y dan origen y son necesarias.
<br><br>
Las finalidades (1) y (2) no son necesarias, pero son importantes para ofrecerle a través de campañas de mercadotecnia, publicidad y prospección comercial, bienes, productos y servicios exclusivos, por lo que usted tiene derecho a oponerse, o bien, a revocar su consentimiento para que Honda Corregidora  deje de tratar sus datos personales para dichas finalidades, de acuerdo al procedimiento señalado en el aparatado IV o V del presente Aviso de Privacidad.
<br><br>
Se hace de su conocimiento que cuenta con un plazo de 5 días hábiles para manifestar su negativa respecto al tratamiento de sus datos personales de identificación, en relación con las finalidades del inciso (b) de este apartado, conforme el mecanismo establecido en el apartado IV del presente Aviso de Privacidad.
<br><br>

 </li>
 
<h4><strong>IV. OPCIONES PARA LIMITAR EL USO O DIVULGACIÓN DE SUS DATOS PERSONALES.</strong></h4><br>

Si usted desea dejar de recibir mensajes de mercadotecnia, publicidad o de prospección comercial, puede hacerlo valer a través del correo electrónico avisodeprivacidad@grupofame.com o si lo prefiere, accediendo a nuestro sitio WEB www.famecorregidora.com  sección Aviso de Privacidad o al teléfono 01 800 670 8386.
<br><br>
<h4><strong>V. SOLICITUD DE ACCESO, RECTIFICACIÓN, CANCELACIÓN U OPOSICIÓN DE DATOS PERSONALES (DERECHOS ARCO) Y REVOCACIÓN DE CONSENTIMIENTO.</strong></h4><br>

Usted tiene en todo momento el derecho de Acceder, Rectificar, Cancelar u Oponerse al tratamiento que le damos a sus datos personales, así como Revocar su consentimiento; derechos que podrá hacer valer enviando un correo electrónico a  avisodeprivacidad@grupofame.com, para que le sea proporcionado el formato de Solicitud de Derechos ARCO, mismo que deberá presentar llenado en el domicilio del Responsable, debiendo adjuntar una copia de su identificación oficial para acreditar su titularidad. La respuesta a su solicitud de Derechos ARCO se le hará llegar al correo electrónico que haya proporcionado, dentro del término de 20 días hábiles contados a partir de la recepción de dicha solicitud. El ejercicio de los Derechos ARCO, será gratuito, en su caso, el titular debe de cubrir los gastos de envío, reproducción y/o certificación de documentos, sin embargo, si el titular ejerce el mismo derecho en un periodo no mayor de 12 meses, la respuesta a la solicitud tendrá un costo que no excederá de 3 unidades de cuenta vigente en la Ciudad de México.
<br><br>
En caso de no estar de acuerdo en el tratamiento de sus datos personales, puede acudir ante el INAI.
<br><br>

<h4><strong>VI. TRANSFERENCIAS DE DATOS PERSONALES.</strong></h4><br>

<li>Transferencia sin necesidad de consentimiento:<br>
Se le informa que a fin de dar cumplimiento a las finalidades establecidas en el inciso (a), apartado III del presente Aviso de Privacidad, sus datos personales pueden ser transferidos y tratados dentro y fuera de los Estados Unidos Mexicanos por personas distintas a Honda Corregidora. En este sentido y con fundamento en La Ley y su Reglamento, sus datos personales podrán ser transferidos sin necesidad de su consentimiento a: (i) A terceros proveedores de servicios, única y exclusivamente para el cumplimiento de las finalidades establecidas en el inciso (a) del apartado III del presente Aviso de Privacidad; (ii) A terceros para realizar gestiones de cobranza extrajudicial y judicial en caso de incumplimiento, (iii) A terceros proveedores de servicios para la investigación y aprobación, en su caso, del crédito correspondiente para el cumplimiento de las finalidades establecidas en el inciso (a) del apartado III del presente Aviso de Privacidad; (iv) A Honda de México S.A. de C.V. para el establecimiento de las condiciones de garantía, campañas de seguridad y servicio (llamada/recall) y actualizaciones técnicas; (v) A las Autoridades competentes en materia de control vehicular; (vi) A instituciones financieras, bancarias y crediticias, para la solicitud de otorgamiento de créditos por el servicio contratado por el cliente; (vii) A aseguradoras para el otorgamiento de pólizas de seguros por el servicio contratado por el cliente; (viii) A Honda de México S.A. de C.V. para cumplir con las obligaciones de extensión de garantía. (ix) A empresas filiales de Honda Corregidora que ofrezcan productos y servicios del portafolio de marcas del conjunto de empresas que integran a Grupo Fame.<br><br>
</li>

<li>Transferencias con consentimiento:<br>
Honda Corregidora, podrá transferir sus datos personales de identificación a (i) Honda de México S.A. de C.V. para administrar el programa de lealtad propio de la marca; (ii) A Honda de México S.A. de C.V. para la evaluación y seguimiento del nivel de satisfacción de nuestros clientes. (iii) Departamento de Contact y Call Center de Honda Corregidora que considere para la realización de estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios; dar seguimiento a la relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de los productos y servicios. A la firma del presente Aviso de Privacidad nos otorga su consentimiento para transferir sus datos personales de identificación. Si usted no desea que sus datos personales sean transferidos a dichos terceros, puede manifestar su negativa conforme al procedimiento establecido en el apartado IV del presente Aviso de Privacidad.</li><br><br>

<h4><strong>VII. USO DE COOKIES</strong></h4><br>

Honda Corregidora, le informa que no utiliza “cookies y web beacons" para obtener información personal de usted de manera automática. Los datos personales que recabamos de manera electrónica, así como las finalidades del tratamiento se encuentran establecidos en el presente Aviso de Privacidad. ESTABLECER EN CASO DE QUE NO LAS USE, EN CASO CONTRARIO SE DEBERÁ ESTABLECER QUE SI LAS USAN Y EL MECANISMO PARA DESABILITARLAS.<br><br>

<h4><strong>VIII. MODIFICACIÓN AL AVISO DE PRIVACIDAD.</strong></h4><br>

Este Aviso de Privacidad podrá ser modificado; dichas modificaciones podrán consultarse a través de los siguientes medios:<br><br>

<li>
1.	Nuestra página de internet www.famecorregidora.com
</li>
 
<li>
2.	Avisos visibles en nuestras instalaciones; o,
</li>
<li>
3.	Cualquier otro medio de comunicación oral, impreso o electrónico que se determine para tal efecto.
</li>

 <br>
<p align="center">
Última fecha de actualización 21 de abril de 2018
</p>


						</p>
					</div>
				</div>

				<!-- services-box -->
				<div class="services-box">
					<div class="container">
						<div class="row">

							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon2" href="arco.pdf" target="_blank"><i class="fa fa-download"></i></a>
									<div class="services-post-content">
										<h4>Descarga</h4>
										<p>Haz clic en el ícono para descargar nuestro archivo de <strong>Derechos ARCO</strong></p>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
								</div>
							</div>

						</div>
					</div>
					<img class="shadow-image" alt="" src="images/shadow.png">
				</div>

			</div>

<?php include('contenido/footer.php'); ?>

</body>
</html>